﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PencaMultiligaMVC.Startup))]
namespace PencaMultiligaMVC
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
